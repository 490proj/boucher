
#include "Balls.h"
#include "SoundEngine.h"

class Physics {
private:
	BallCollection* m_b;
	SoundEngine* m_s;
public:
	Physics(BallCollection* b, SoundEngine* s); //:m_b(b), m_e(s) {};
	
	/** Cette fonction replace le triangle sur la table*/
	void ResetTriangle();

	/** Cette fonciton fait un pas de temps de simulation */
	void Simulate1StepBalls();
			
};