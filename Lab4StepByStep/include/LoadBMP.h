
//Include for fixed length integer types
#include <cstdint>

//Using namespace to avoid clashes with wingdi.h somehow included by glew.h, but these types are actually defined in wingdi.h
namespace Rivest {

//The pragma pack compiler directive, in Visual Studio, for the structure to have the exact required size in memory
//This may slow down access to the data in the structure in practice, today, we often want 32-bits byte alignment. 
#pragma pack(push, 1)
typedef struct  {
	char signature[2];
	uint32_t filesize;
	char reserved[4];
	uint32_t offset;
} BITMAPFILEHEADER; //First 14 bytes
#pragma pack(pop)


#pragma pack(push, 1)
typedef struct  {
	uint32_t headerSize;
	int32_t width;
	int32_t height;
	uint16_t planeCount;
	uint16_t bitsPerPixel;
	uint32_t compressionMode;
	uint32_t rawSize;
	int32_t horzResolution;
	int32_t vertResolution;
	uint32_t paletteColorCount;
	uint32_t importantColorCount;
} BITMAPINFOHEADER; //Standard header up to BITMAPINFOHEADER
#pragma pack(pop)


unsigned char* loadBMP(const char *filename, int& width, int& height);
GLuint  LoadTexFromBMP(const char* filename);

}

using namespace Rivest;