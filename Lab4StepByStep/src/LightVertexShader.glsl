#version 330

uniform mat4 viewMat;	//Full view transformation
uniform mat4 objMat;	//Transformation for this object only
uniform mat4 normMat;	//Rotations for this object only (for normals)  
 

in vec4 vVertex;		//Vertex location in object coordinate
in vec2 vTexCoord0;		//Vertex texture coordinate
in vec4 vNormal;		//Vertex attached normal (in object coordinate) 

smooth out vec2 vVarTexCoords0; //Interpolate texture coordinates for each fragment
smooth out vec4 vWorldCoords;	//Interpolate world coordinates for each fragment
flat out vec3 vFlatNormals;		//Copy normals flat (using the last vertex of a triangle)

void main(void) {
	vVarTexCoords0 = vTexCoord0;	//Use texture coordinate as is
	vWorldCoords = objMat*vVertex;	//Move the object in world coordinates
	vFlatNormals = normalize((normMat*vNormal).xyz);//Apply object rotation to the surface normal
	gl_Position = viewMat*vVertex;	//Transform object into OpenGL unit cube coordinate
}
