
#include "SoundEngine.h"

SoundEngine::SoundEngine(BallCollection* B, Users* U)
{
	m_B = B;
	m_U = U;
}

SoundEngine::~SoundEngine()
{
	engine->stopAllSounds();
	engine->drop();
}

bool SoundEngine::SoundLoad() {
	//Initiate the sound engine. Return an error if not loaded correctly
	engine = irrklang::createIrrKlangDevice();
	//Check if successful
	if (!engine) { // error starting up the engine
		std::cout << "Could not start engine!" << std::endl;
		return false; }
	else { std::cout << "Engine initiated successfully!" << std::endl; }
	
	//Set the engine volume (master volume)
	engine->setSoundVolume(SE_MVol);
	
	//Load the songs and place them in the array
	track0 = engine->addSoundSourceFromFile("../Assets/CC-CFRO.ogg");
	track1 = engine->addSoundSourceFromFile("../Assets/BR-SS.ogg");
	track2 = engine->addSoundSourceFromFile("../Assets/DF-BR.ogg");
	track3 = engine->addSoundSourceFromFile("../Assets/DS(f).ogg");
	track4 = engine->addSoundSourceFromFile("../Assets/G-SB.ogg");
	track5 = engine->addSoundSourceFromFile("../Assets/ID-R90.ogg");
	track6 = engine->addSoundSourceFromFile("../Assets/SPARTA.ogg");

	songsList[0] = track0->getName();
	songsList[1] = track1->getName();
	songsList[2] = track2->getName();
	songsList[3] = track3->getName();
	songsList[4] = track4->getName();
	songsList[5] = track5->getName();
	songsList[6] = track6->getName();

	//Load the sounds
	ball_Ball = engine->addSoundSourceFromFile("../Assets/BWA.ogg");
	ball_Wall = engine->addSoundSourceFromFile("../Assets/WAA(q).ogg");
	ball_Pole = engine->addSoundSourceFromFile("../Assets/LilJenk.ogg");
	ball_Hole = engine->addSoundSourceFromFile("../Assets/ALIA.ogg");
	ball_R2M = engine->addSoundSourceFromFile("../Assets/RockKnuckles.ogg");
	
	//Step variation sounds list
	stepSounds[0] = engine->addSoundSourceFromFile("../Assets/stepVar1.ogg")->getName();
	stepSounds[1] = engine->addSoundSourceFromFile("../Assets/stepVar2.ogg")->getName();
	stepSounds[2] = engine->addSoundSourceFromFile("../Assets/stepVar3.ogg")->getName();
	stepSounds[3] = engine->addSoundSourceFromFile("../Assets/stepVar4.ogg")->getName();
	
	player_ScoreG = engine->addSoundSourceFromFile("../Assets/WinHorn.ogg");
	player_ScoreB = engine->addSoundSourceFromFile("../Assets/LoseHorn.ogg");
	player_Win = engine->addSoundSourceFromFile("../Assets/OMS.ogg");
	player_Lose = engine->addSoundSourceFromFile("../Assets/DAWAE.ogg");
	
	//For debugging purposes, play a beeping sound at a set interval
	ball_Roll = engine->addSoundSourceFromFile("../Assets/Beep.ogg");

	return true;
}

void SoundEngine::keyControls(unsigned char key) {
	switch (key) {
	case 'y': SE_JukeVol += 0.05;	//Volume up 
		if (SE_JukeVol > 1.00f) { 
			SE_JukeVol = 1.00f; 
		}
		current_track->setVolume(SE_JukeVol);
		break;
	case 'h': SE_JukeVol -= 0.05;	//Volume down
		if (SE_JukeVol < 0.00f) { 
			SE_JukeVol = 0.00f; 
		}
		current_track->setVolume(SE_JukeVol);
		break;
	case 'k':		//Stop track
		//trackName = new const char(songsList[SE_index_songsList - 1]);
		if(!current_track->isFinished()){
			current_track->stop();
		} else {
			current_track = engine->play2D(songsList[SE_index_songsList - 1], false, true, true, ESM_AUTO_DETECT, true);
			current_track->setPlayPosition(0);
			current_track->setIsPaused(false);
		}
		break;
	case 'i':		//Pause/play track
		if(!current_track->getIsPaused()){		//If it isn't paused: get the playback time, pause the sound and set the playback time to what was pulled
			playPos = current_track->getPlayPosition();
			current_track->setIsPaused(true);
			current_track->setPlayPosition(playPos);
		} else {	//If the track is not playing: play the sound
			current_track->setIsPaused(false);
		}
		break;
	case 'j':		//Play previous track
		if (current_track->getPlayPosition() > 5000) {	//If track has been playing for more than 5 seconds, return to start
			current_track->setIsPaused(true);
			current_track->setPlayPosition(0);
			current_track->setIsPaused(false);
			//current_track = engine->play2D(current_track->getSoundSource(), false, false, true); 
		}
		else {		//Otherwise, set the current track to the previous one
			SE_index_songsList -= 2;
			if (SE_index_songsList < 0) {
				SE_index_songsList += SE_SongsCount; 
			}
			current_track->stop();
			//trackName = new const char(songsList[SE_index_songsList]);
			current_track = engine->play2D(songsList[SE_index_songsList], true, false, true);
			SE_index_songsList = (SE_index_songsList + 1) % SE_SongsCount;
		}
		break;
	case 'l':		//Play next track
		current_track->stop();
		//trackName = new const char(songsList[SE_index_songsList]);
		current_track = engine->play2D(songsList[SE_index_songsList - 1], true, false, true);
		SE_index_songsList = (SE_index_songsList + 1) % SE_SongsCount;
		break;
	case 8:		//[backspace] KILL COMMAND
		engine->stopAllSounds();
		break;
		
	}

	//Update the engine to make sure it commits the changes
	engine->update();
	return;
}


void SoundEngine::update(vec4 camera_pos, vec3 camera_angle) {

	//***** Mettre � jour l'information du joueur
	vec3df position = vec3_df(camera_pos); // Position of the listener
	vec3df lookDirection = vec3_df(camera_angle);	// The direction the listener looks into
	vec3df velPerSecond(0, 0, 0);    // Only relevant for doppler effects
	vec3df upVector(0, 1, 0);        // Where 'up' is in your 3D scene

	engine->setListenerPosition(position, lookDirection, velPerSecond, upVector);

	//*****Appliquer les modifications au moteur
	engine->update();
}

void SoundEngine::SoundEvents(vec3 Position, int se_sound_event) {//ball collection is m_B
	if(se_sound_event != 0){
		sndLoc = vec3_df(Position);
		switch (se_sound_event) {
			//***** BALL EVENTS
		case 1: //When 2 balls come in contact with each other
			engine->play3D(ball_Ball, sndLoc, false, false, true, false)->setMinDistance(5.0);	//5.0 not final value; will need testing to see how loud it is
			break;

		case 2:	//When a ball hits a wall
			engine->play3D(ball_Wall, sndLoc, false, false, true, false)->setMinDistance(5.0);
			break;

		case 3:	//When the pole strikes the ball
			engine->play3D(ball_Pole, sndLoc, false, false, true, false)->setMinDistance(5.0);
			break;

		case 4:	//When the ball enters a pouch
			engine->play3D(ball_Hole, sndLoc, false, false, true, false)->setMinDistance(5.0);
			//sleep(100);
			engine->play3D(ball_R2M, vec3df(0.0f, 1.0f, 0.0f), false, false, true, false)->setMinDistance(5.0);	//The centre of the pool table
			break;

		case 5:	//When the ball rolls

			std::cout << "Insert rolling sound here" << std::endl;
			//Sound generation will be done after the debugging
			break;

		case 6:	//When the ball rotates (curve ball)


			std::cout << "Insert spinning sound here" << std::endl;
			//Sound generation will be done after the debugging
			break;

			//***** PLAYER EVENTS
		case 7:	//When the player moves [called by keyControls()]
			//Set the step sound from stepSounds[4]
			sndLoc.Y = 0.0f;
			player_Step = engine->play2D(stepSounds[SE_index_stepVars], false, false, true, ESM_AUTO_DETECT, false);
			//player_Step = engine->play3D(stepSounds[SE_index_stepVars], sndLoc, false, false, true, ESM_AUTO_DETECT, false);
			//player_Step->setMinDistance(5.0f);
			SE_index_stepVars = (SE_index_stepVars + 1) % SE_stepVars;
			break;
		}

		if (false) {	//Temporarily set to false; they will only be triggered by another event
			//When the player scores
			//After the ball enters the pouch, play a small victory sound
			if (/*sunk his ball*/0) {
				engine->play3D(player_ScoreG, sndLoc);
			}
			else {
				engine->play3D(player_ScoreB, sndLoc);
			}

			//At the end of the game
			if (/*player won*/0) {
				engine->play3D(player_Win, sndLoc, false, false, true, false);
			}
			else {
				engine->play3D(player_Lose, sndLoc, false, false, true, false);
			}
		}

	} else {
		if (SE_startUp) {
			current_track = engine->play2D(songsList[SE_index_songsList], false, true, true, irrklang::ESM_AUTO_DETECT, false);
			SE_index_songsList = (SE_index_songsList + 1) % SE_SongsCount;
			current_track->setVolume(SE_JukeVol);
			current_track->setIsPaused(false);
			//Use this block if starting/restarting the jukebox
			SE_startUp = false;
		}
		else {
			if (current_track->isFinished()) {
				current_track = engine->play2D(songsList[SE_index_songsList], false, true, true, irrklang::ESM_AUTO_DETECT, true);
				current_track->setVolume(SE_JukeVol);
				current_track->setIsPaused(false);
				SE_index_songsList = (SE_index_songsList + 1) % SE_SongsCount;
			}
		}
	}
}


