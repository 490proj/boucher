#version 330 core

uniform vec4 vColor; 

void main()
{
    gl_FragColor = vColor;
}
