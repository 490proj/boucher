# Boucher

Fonctions: 

SoundLoad()
-> Génère le moteur et alloue de l'espace pour les sons ainsi que leurs propriétés
	- returns bool true si le moteur et les sons sont bien initialisés

keyControls(key)
-> Prends en charge les événements causés par une frappe
	- w,a,s,d: Incrémente le nombre d'appels et joue un de 4 sons de pas à chaque (nombre déterminé) de pas
	- y, h : Augmente et diminue respectivement le volume du jukebox
	- i: Pause/reprends la chanson du jukebox au même endroit
	- j: Arrête/recommence la chanson au jukebox
	- j, l: Fais jouer la chanson précédente/suivante (respectivement) du jukebox
	- Backspace: Arrête tous les sons du moteur

update()
-> Mets à jour les sons du moteur
	- Repositionne le joueur par rapport aux sons ainsi que son orientation
	- Mets à jour la position de tous les sons 3D

SoundEvents(vec3() position, int soundEvent)
-> Prends en charge les événements
	-case 0: Faire jouer la prochaine chanson dans la liste
	-case 1: Collision entre 2 balles
		> Fais jouer un son au lieu de collision
	-case 2: Collision entre une balle et un mur
		> Fais jouer un son au lieu de collision
	-case 3: Quand la balle blanche est frappée
		> Fais jouer un son au lieu de collision
	-case 4: Quand la balle entre dans une des poches
		> Fais jouer un son à la poche et le son de la balle qui entre dans le mécanisme de la table
		> Vérifies si le joueur entre sa balle ou la mauvaise. Joues un son selon le cas
	-case 5: 
		> 
	-case 6:
		>
	-case 7:
		>
	-case 8:
		>
	-case 9:
		>
	-case 10:
		>
		
		
		
		
		
		
		
		
		